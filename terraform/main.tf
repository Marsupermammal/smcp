provider "aws" {
  region = "${var.aws_region}"
}

module "acct" {
  source = "./modules/acct"
  aws_region = "${var.aws_region}"
  environment = "${var.environment}"
}

module "vpc" {
  source = "./modules/vpc"
  aws_region = "${var.aws_region}"
  cloudwatch_iam = "${module.acct.flow_log}"
  environment = "${var.environment}"
  dmz_count = "${var.dmz_count}"
  pub_count = "${var.pub_count}"
  priv_count = "${var.priv_count}"
}

resource "aws_security_group" "web_access" {
  name = "web_access"
  vpc_id = "${module.vpc.vpc_id}"

  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "web" {
  ami = "${data.aws_ami.ubuntu.id}"
  availability_zone = "${data.aws_availability_zones.available.names[0]}"
  instance_type = "${var.instance_type}"
  vpc_security_group_ids = ["${module.vpc.baselinesg}", "${aws_security_group.web_access.id}"]
  subnet_id = "${module.vpc.pub_subnets[0]}"
  associate_public_ip_address = true
  user_data = "${data.template_file.userdata.rendered}"
  tags {
    Name = "${var.environment}-web"
    Owner = "${var.owner}"
  }
}
