variable "aws_region" {}
variable "environment" {}
variable "dmz_count" {}
variable "pub_count" {}
variable "priv_count" {}
variable "instance_type" {}
variable "owner" {}
