#!/bin/bash -ex
apt-get update -y
apt-get install -y wget git
wget https://packages.chef.io/stable/ubuntu/12.04/chefdk_0.18.26-1_amd64.deb
dpkg -i chefdk_0.18.26-1_amd64.deb

git clone https://bitbucket.org/Marsupermammal/smcp.git

chef-apply /smcp/smcp/recipes/default.rb
