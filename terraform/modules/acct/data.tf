data "template_file" "cloud_trail_policy" {
  template = "${file("${path.module}/templates/cloud_trail_policy.json.tpl")}"
  vars {
    bucket_name = "${var.environment}-cloudtrail"
  }
}

data "template_file" "tf-VPCFlowLogsAccess" {
  template = "${file("${path.module}/templates/tf-VPCFlowLog.json.tpl")}"
}

data "template_file" "tf-ReadOnlyAccess" {
  template = "${file("${path.module}/templates/tf-ReadOnlyAccess.json.tpl")}"
}

data "template_file" "tf-AdminAccess" {
  template = "${file("${path.module}/templates/tf-AdminAccess.json.tpl")}"
}

data "template_file" "flowlog-assumerole" {
  template = "${file("${path.module}/templates/assume_role_policy.json.tpl")}"
  vars {
    aws_service = "vpc-flow-logs.amazonaws.com"
  }
}

data "template_file" "ec2-assumerole" {
  template = "${file("${path.module}/templates/assume_role_policy.json.tpl")}"
  vars {
    aws_service = "ec2.amazonaws.com"
  }
}
