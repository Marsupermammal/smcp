variable "aws_region" {}
variable "environment" {}
variable "readonlyname" {
  default = "read-only"
}
