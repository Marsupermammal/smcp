output "read_only_profile"{
  value = "${aws_iam_instance_profile.read_only_role_profile.name}"
}

output "flow_log" {
  value = "${aws_iam_role.flowlogrole.arn}"
}

output "remote_state_bucket" {
  value = "${aws_s3_bucket.remote_state.id}"
}

output "cloud_trail_id" {
  value = "${aws_cloudtrail.trail.id}"
}
