provider "aws" {
  region = "${var.aws_region}"
}

resource "aws_cloudtrail" "trail" {
  name = "${var.environment}-cloudtrail"
  s3_bucket_name = "${aws_s3_bucket.cloud_trail.id}"
  enable_logging = true
  include_global_service_events = true
}

resource "aws_s3_bucket" "cloud_trail" {
  bucket = "${var.environment}-cloudtrail"
  acl = "log-delivery-write"
  force_destroy = true
  policy = "${data.template_file.cloud_trail_policy.rendered}"
}

resource "aws_s3_bucket" "remote_state" {
  bucket = "${var.environment}-remote-state"
  force_destroy = true
}

resource "aws_iam_role" "flowlogrole" {
    name = "${var.environment}-flow_logs_role"
    assume_role_policy = "${data.template_file.flowlog-assumerole.rendered}"
}

resource "aws_iam_role_policy" "flowlogs" {
  name = "${var.environment}-tf-VPCFlowLog"
  role =  "${aws_iam_role.flowlogrole.id}"
  policy = "${data.template_file.tf-VPCFlowLogsAccess.rendered}"
}

resource "aws_iam_policy" "ReadOnlyAccess" {
  name = "${var.environment}-tf-ReadOnlyAccess"
  policy = "${data.template_file.tf-ReadOnlyAccess.rendered}"
}

resource "aws_iam_policy_attachment" "RO-attach" {
  name = "${var.environment}-ReadOnlyAccess-attach"
  roles = ["${aws_iam_role.read_only_role.name}"]
  policy_arn = "${aws_iam_policy.ReadOnlyAccess.arn}"
}

resource "aws_iam_role" "read_only_role" {
  name = "${var.environment}-tf-ReadOnlyAccess"
  assume_role_policy = "${data.template_file.ec2-assumerole.rendered}"
}

resource "aws_iam_instance_profile" "read_only_role_profile" {
  name = "${var.environment}-read-only-profile"
  roles = ["${aws_iam_role.read_only_role.name}"]
}
