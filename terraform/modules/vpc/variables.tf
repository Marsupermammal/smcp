variable "aws_region" {}
variable "cloudwatch_iam" {}
variable "environment" {}
variable "dmz_subnet_cidr" {
  type = "map"
  default = {
    "0" = "172.16.0.0/24"
    "1" = "172.16.1.0/24"
    "2" = "172.16.2.0/24"
  }
}
variable "pub_subnet_cidr" {
  type = "map"
  default = {
    "0" = "172.16.3.0/24"
    "1" = "172.16.4.0/24"
    "2" = "172.16.5.0/24"
  }
}
variable "priv_subnet_cidr" {
  type = "map"
  default = {
    "0" = "172.16.6.0/24"
    "1" = "172.16.7.0/24"
    "2" = "172.16.8.0/24"
  }
}
variable "vpc_cidr_block" {
  default = "172.16.0.0/16"
}
variable "dmz_count" {}
variable "pub_count" {}
variable "priv_count" {}
