output "pub_subnets" {
  value = ["${aws_subnet.pub_subnet.*.id}"]
}

output "dmz_subnets" {
  value = ["${aws_subnet.dmz_subnet.*.id}"]
}

output "priv_subnets" {
  value = ["${aws_subnet.priv_subnet.*.id}"]
}

output "azs" {
  value = "${data.aws_availability_zones.available.names}"
}

output "vpc_id" {
  value = "${aws_vpc.vpc.id}"
}

output "baselinesg" {
  value = "${aws_security_group.baseline.id}"
}

output "log_group_arn" {
  value = "${aws_cloudwatch_log_group.log_group.arn}"
}
