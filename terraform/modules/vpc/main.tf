provider "aws" {
  region = "${var.aws_region}"
}

resource "aws_vpc" "vpc" {
  cidr_block = "${var.vpc_cidr_block}"
  enable_dns_support = true
}

resource "aws_internet_gateway" "igw" {
  vpc_id = "${aws_vpc.vpc.id}"
}

resource "aws_cloudwatch_log_group" "log_group" {
  name = "${var.environment}_vpc_log_group"
}

resource "aws_flow_log" "flow_log" {
  log_group_name = "${aws_cloudwatch_log_group.log_group.name}"
  iam_role_arn = "${var.cloudwatch_iam}"
  vpc_id = "${aws_vpc.vpc.id}"
  traffic_type = "ALL"
  depends_on = ["aws_cloudwatch_log_group.log_group"]
}

resource "aws_vpc_endpoint" "s3e" {
    vpc_id = "${aws_vpc.vpc.id}"
    route_table_ids = ["${aws_route_table.private-to-nat-gateway.*.id}","${aws_route_table.pub-rt.id}"]
    service_name = "com.amazonaws.${var.aws_region}.s3"
}

resource "aws_subnet" "dmz_subnet" {
  count = "${var.dmz_count}"
  vpc_id = "${aws_vpc.vpc.id}"
  availability_zone = "${data.aws_availability_zones.available.names[count.index]}"
  cidr_block =  "${lookup(var.dmz_subnet_cidr, count.index)}"
}

resource "aws_subnet" "pub_subnet" {
  count = "${var.pub_count}"
  vpc_id = "${aws_vpc.vpc.id}"
  availability_zone = "${data.aws_availability_zones.available.names[count.index]}"
  cidr_block =  "${lookup(var.pub_subnet_cidr, count.index)}"
}

resource "aws_subnet" "priv_subnet" {
  count = "${var.priv_count}"
  vpc_id = "${aws_vpc.vpc.id}"
  availability_zone = "${data.aws_availability_zones.available.names[count.index]}"
  cidr_block =  "${lookup(var.priv_subnet_cidr, count.index)}"
}

resource "aws_eip" "nat_gateway" {
  count = "${var.dmz_count}"
  vpc = true
}

resource "aws_nat_gateway" "gateway" {
  count = "${var.dmz_count}"
  allocation_id = "${element(aws_eip.nat_gateway.*.id, count.index)}"
  subnet_id = "${element(aws_subnet.dmz_subnet.*.id, count.index)}"
}

resource "aws_route_table" "pub-rt" {
  vpc_id = "${aws_vpc.vpc.id}"
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.igw.id}"
  }
}

resource "aws_route_table" "private-to-nat-gateway" {
  count = "${var.dmz_count}"
  vpc_id = "${aws_vpc.vpc.id}"
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${element(aws_nat_gateway.gateway.*.id, count.index)}"
  }
}

resource "aws_route_table_association" "dmz-rta" {
  count = "${var.dmz_count}"
  subnet_id = "${element(aws_subnet.dmz_subnet.*.id, count.index)}"
  route_table_id = "${aws_route_table.pub-rt.id}"
  depends_on = ["aws_subnet.dmz_subnet"]
}

resource "aws_route_table_association" "pub-rta" {
  count = "${var.pub_count}"
  subnet_id = "${element(aws_subnet.pub_subnet.*.id, count.index)}"
  route_table_id = "${aws_route_table.pub-rt.id}"
  depends_on = ["aws_subnet.pub_subnet"]
}

resource "aws_route_table_association" "priv-to-nat-gateway-rta" {
  count = "${var.priv_count}"
  subnet_id = "${element(aws_subnet.priv_subnet.*.id, count.index)}"
  route_table_id = "${element(aws_route_table.private-to-nat-gateway.*.id, count.index)}"
  depends_on = ["aws_subnet.priv_subnet"]
}

resource "aws_security_group" "baseline" {
  vpc_id = "${aws_vpc.vpc.id}"
  ingress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    self = true
  }
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
