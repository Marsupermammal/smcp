#
# Cookbook Name:: smcp
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
package 'apache2'

service 'apache2' do
  action [:start, :enable]
end

file '/var/www/html/index.html' do
  content '<html><body><h1>Automation for the People</h1></body></html>'
  mode '0755'
end
