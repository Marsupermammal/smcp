name             'smcp'
maintainer       'Phil Watts'
maintainer_email 'pwatts217@gmail.com'
license          'All rights reserved'
description      'Installs/Configures smcp'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'
