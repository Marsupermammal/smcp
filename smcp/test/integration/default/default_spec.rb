# Encoding: utf-8

require 'inspec'
require 'open-uri'

describe package('apache2') do
  it { should be_installed }
end

describe upstart_service('apache2') do
  it { should be_running }
  it { should be_installed }
  it { should be_enabled }
end

describe port(80) do
  it { should be_listening }
  its('processes') {should include 'apache2' }
end

describe file('/var/www/html/index.html') do
  it { should be_file }
  it { should be_readable }
  its('content') { should match(%r{<html><body><h1>Automation for the People<\/h1><\/body><\/html>}) }
end
