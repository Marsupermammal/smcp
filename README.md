# Stelligent Mini Candidate Project

## Author
Phil Watts

## Requirements/Assumptions:
1. Terraform installed (https://terraform.io)
2. AWS credentials configured (http://docs.aws.amazon.com/cli/latest/userguide/cli-chap-getting-started.html)

## Usage
```
$ cd terraform
$ terraform get
$ terraform apply
```
